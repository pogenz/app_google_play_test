class AppImages {
  static const String pathGame = "assets/images/games/";
  static const String pathWidget = "assets/images/widgets/";
  static const String APP_BAR_BACKGROUND = "assets/images/app_bar_background.png";
  static const String LOGIN_BACKGROUND = "assets/images/login_background.png";
  static const String IC_BACK = "assets/images/ic_back.png";
  static const String LOGO = pathWidget + "gg_logo.png";
  static const String FACE = pathWidget + "fb.png";
  static const String YOUTUBE = pathWidget + "youtubeLogo.png";
  static const String SPOTIFY = pathWidget + "spotifyLogo.png";
  static const String CHROME = pathWidget + "chromeLogo.png";
  static const String TIKTOK = pathWidget + "tiktokLogo.jpeg";
  static const String SHOPEE = pathWidget + "shopeeLogo.png";
  static const String GMAIL = pathWidget + "gmailLogo.png";
  static const String MESS = pathWidget + "messLogo.jpeg";
  static const String ASPHALT = pathGame + "asphalt.WEBP";
  static const String ASPHALTLOGO = pathGame + "asphaltLogo.png";
  static const String LQ = pathGame + "lq1.png";
  static const String LQLOGO = pathGame + "lqLogo.png";
  static const String PUBGLOGO = pathGame + "pubgLogo.png";
  static const String PUBG = pathGame + "pubg.png";
  static const String GENSHINLOGO = pathGame + "genshinLogo.png";
  static const String GENSHIN = pathGame + "genshin.png";




}
