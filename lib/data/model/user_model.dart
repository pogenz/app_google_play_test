class UserModel {
  final int? id;
  final String? username;
  final String? password;
  final String? email;
  UserModel({this.id, this.username, this.email, this.password});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic> {
      'UserName':username,
      'password': password,
      'email': email,
    };
    return map;
  }
  // @override
  // List<Object> get props => [id,username,password,email];
}