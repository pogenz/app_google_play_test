import 'package:dashboard_app/res/resources.dart';
import 'package:dashboard_app/routes.dart';
// import 'package:dashboard_app/ui/widget/widget.dart';
import 'package:dashboard_app/utils/shared_preference.dart';
import 'package:flutter/material.dart';
import 'package:scale_size/scale_size.dart';
class CustomAppbar extends StatelessWidget with PreferredSizeWidget{
  const CustomAppbar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: AppColors.white,
      automaticallyImplyLeading: false,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
              height: 50.sh,
              width: 50.sw,
              child: Padding(
                padding: EdgeInsets.only(right: 10.sw),
                child: Image.asset(
                  AppImages.LOGO,
                  fit: BoxFit.fitWidth,
                ),
              )),
          Text(
            "Guugle Play",
            style: TextStyle(
                fontSize: 25,
                color: AppColors.gray,
                fontWeight: FontWeight.bold),
          ),
        ],
      ),
      actions: <Widget>[
        Padding(
          padding: EdgeInsets.only(right: 15.sw),
          child: PopupMenuButton<int>(
              constraints: BoxConstraints(
                minWidth: 300.sw,
                minHeight: 300.sw
              ),
              icon: Icon(
                Icons.menu,
                color: AppColors.gray,
              ),
              iconSize: 30,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              onSelected: (item) => onSelected(context, item),
              itemBuilder: (_context) =>
              [
                PopupMenuItem(
                    child: Row(
                      children: [
                        Icon(
                          Icons.person,
                          color: AppColors.gray,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10.sw),
                          child: Text("User name"),
                        ),
                      ],
                    )),
                PopupMenuItem(
                    value: 0,
                    child: Container(
                      alignment: Alignment.centerRight,
                      child: ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                  AppColors.white),
                              elevation: MaterialStateProperty.all(1),
                              shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                      borderRadius:
                                      BorderRadius.circular(15)))),
                          onPressed: () {
                            Navigator.of(context)
                                .pushNamed(Routes.profile);
                          },
                          child: Text(
                            "Quản lí tài khoản của bạn",
                            style: TextStyle(color: AppColors.gray),
                          )),
                    )),
                PopupMenuDivider(
                  height: 2,
                ),
                PopupMenuItem<int>(value: 1, child: Text("Profile")),
                PopupMenuItem<int>(value: 2, child: Text("Profile")),
                PopupMenuItem<int>(value: 3, child: Text("Profile")),
                PopupMenuDivider(),
                PopupMenuItem<int>(
                    value: 4,
                    child: Row(
                      children: [
                        Icon(
                          Icons.logout,
                          color: AppColors.gray,
                        ),
                        Padding(
                            padding:
                            EdgeInsets.symmetric(horizontal: 10.sw),
                            child: Text("Log out")),
                      ],
                    )),
                PopupMenuDivider(),
                PopupMenuItem(
                    child: Container(
                      alignment: Alignment.bottomCenter,
                      child: Text(
                        "Chính sách quyền riêng tư    |    Điều khoản dịch vụ",
                        style: TextStyle(fontSize: 15),
                        maxLines: 1,
                      ),
                    ))
              ]),
        )
      ],
      elevation: 0,
    );
  }
  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
  void onSelected(BuildContext context, int item) {
    switch (item) {
      case 0:
      // Navigator.of(context).pushNamed(Routes.profile);
        break;
      case 1 :
        break;
      case 2:
        break;
      case 3:
        break;
      case 4:
        Navigator.pushReplacementNamed(context, Routes.loginScreen);
        SharedPreferenceUtil.clearData();
    }
  }
}
