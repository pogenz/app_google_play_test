import 'package:dashboard_app/blocs/base_bloc/base.dart';
import 'package:dashboard_app/blocs/cubit/cubit.dart';
import 'package:dashboard_app/data/model/model.dart';
import 'package:dashboard_app/res/resources.dart';
import 'package:dashboard_app/routes.dart';
import 'package:dashboard_app/ui/widget/widget.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scale_size/scale_size.dart';
class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<RegisterCubit>(create: (_) => RegisterCubit(), child: RegisterBody(),);
  }
}
class RegisterBody extends StatefulWidget {
  const RegisterBody({Key? key}) : super(key: key);

  @override
  State<RegisterBody> createState() => _RegisterBodyState();
}

class _RegisterBodyState extends State<RegisterBody> {
  GlobalKey<TextFieldState> userNameKey = GlobalKey<TextFieldState>();
  GlobalKey<TextFieldState> emailKey = GlobalKey<TextFieldState>();
  GlobalKey<TextFieldState> passKey = GlobalKey<TextFieldState>();
  GlobalKey<TextFieldState> rePassKey = GlobalKey<TextFieldState>();
  final formkey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return
      BaseScreen(
      loadingWidget: CustomLoading<RegisterCubit>(),
      messageNotify: CustomSnackBar<RegisterCubit>(

      ),
      title: "Đăng ký",
      body: Form(
        key: formkey,
        child: Container(
          color: AppColors.white,
          child: Column(
            children: [
              BlocListener<RegisterCubit, BaseState>(
                listener: (context,state){
                  // if (state is ErrorState)
                },
                child: Container(),
              ),
              Container(
                child: CustomTextInput(
                  validator: (String userName){
                    if (userName.isEmpty)
                      return "Hãy nhập tên đăng nhập!";
                    else
                      return "";
                  },
                  key: userNameKey,
                  margin: EdgeInsets.symmetric(
                    horizontal: 10.sw,
                    vertical: 10.sw
                  ),
                  enableBorder: true,
                  hideUnderline: true,
                  obscureText: false,
                  prefixIcon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.person),
                  ),
                  title: "Tên đăng nhập",
                  hintText: "Nhập tên đăng nhập",
                ),
              ),
              Container(
                child: CustomTextInput(
                  margin: EdgeInsets.symmetric(
                      horizontal: 10.sw,
                      vertical: 10.sw
                  ),
                  validator: (String email){
                    if (email.isEmpty){
                      return "Hãy nhập email!";
                    }
                    else
                      return "";
                  },
                  key: emailKey,
                  enableBorder: true,
                  hideUnderline: true,
                  obscureText: false,
                  prefixIcon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.mail),
                  ),
                  title: "Email",
                  hintText: "Nhập email",
                ),
              ),
              Container(
                child: CustomTextInput(
                  margin: EdgeInsets.symmetric(
                      horizontal: 10.sw,
                      vertical: 10.sw
                  ),
                  validator: (String pass){
                    if (pass.isEmpty){
                      return "Hãy nhập mật khẩu!";
                    }
                    else
                      return "";
                  },
                  key: passKey,
                  enableBorder: true,
                  hideUnderline: true,
                  isPasswordTF: true,
                  prefixIcon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.lock),
                  ),
                  suffixIcon: IconButton(
                    icon: Icon(Icons.visibility),
                    onPressed: (){

                    },
                  ),
                  title: "Mật khẩu",
                  hintText: "Nhập mật khẩu",
                ),
              ),
              Container(
                child: CustomTextInput(
                  margin: EdgeInsets.symmetric(
                      horizontal: 10.sw,
                      vertical: 10.sw
                  ),
                  validator: (String rePass){
                    if (rePass.isEmpty){
                      return "Hãy nhập lại mật khẩu!";
                    }
                    else
                      return "";
                  },
                  enableBorder: true,
                  hideUnderline: true,
                  isPasswordTF: true,
                  key: rePassKey,
                  suffixIcon: IconButton(
                    icon: Icon(Icons.visibility),
                    onPressed: () {

                    },
                  ),
                  prefixIcon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.lock),
                  ),
                  title: "Mật khẩu",
                  hintText: "Nhập mật khẩu",
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: 20.sw
                ),
                child: BaseButton(
                  width: 150.sw,
                  title: "Đăng ký",
                  onTap: () {
                    String username = userNameKey.currentState?.value ?? "";
                    String email = emailKey.currentState?.value ?? "";
                    String pass = passKey.currentState?.value ?? "";
                    String rePass = rePassKey.currentState?.value ?? "";
                    final bool isValidEmail = EmailValidator.validate(email);
                    if (userNameKey.currentState!.isValid || emailKey.currentState!.isValid || passKey.currentState!.isValid || rePassKey.currentState!.isValid){
                      if (isValidEmail == false){
                        CustomShowAlert(context, "Email không đúng");
                      }
                      else if( pass != rePass){
                        CustomShowAlert(context, "Mật khẩu không trùng nhau!");
                      }
                      else{
                        UserModel user = new UserModel(username: username,email: email,password: pass);
                        BlocProvider.of<RegisterCubit>(context).register(user);
                        Navigator.of(context).pushReplacementNamed(Routes.loginScreen);
                      }
                    }
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: 50.sw,
                    bottom: 20.sw,
                ),
                child: Text(
                  "Bạn đã có tài khoản",
                  style: TextStyle(
                    fontSize: 20,
                    color: AppColors.gray,
                  ),
                ),
              ),
              BaseButton(
                width: 150.sw,
                title: "Đăng nhập",
                onTap: () {
                  Navigator.pushNamed(context,Routes.loginScreen);
                },
              )
            ],
          ),
        ),
      ),

    );
  }
}

