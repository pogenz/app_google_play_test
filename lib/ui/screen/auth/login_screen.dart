import 'package:dashboard_app/blocs/base_bloc/base.dart';
import 'package:dashboard_app/res/resources.dart';
import 'package:dashboard_app/routes.dart';
import 'package:dashboard_app/ui/widget/widget.dart';
import 'package:dashboard_app/utils/shared_preference.dart';

// import 'package:dashboard_app/utils/shared_preference.dart';
import 'package:flutter/material.dart';
import 'package:dashboard_app/blocs/cubit/cubit.dart';

// import 'package:dashboard_app/ui/screen/screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scale_size/scale_size.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginCubit>(
        create: (_) => LoginCubit(), child: LoginBody());
  }
}

class LoginBody extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginBody> {
  GlobalKey<TextFieldState> keyName = GlobalKey<TextFieldState>();
  GlobalKey<TextFieldState> keyPass = GlobalKey<TextFieldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      loadingWidget: CustomLoading<LoginCubit>(),
      messageNotify: CustomSnackBar<LoginCubit>(),
      title: "Đăng nhập",
      hiddenIconBack: true,
      body: Container(
        color: AppColors.white,
        child: Column(
          children: [
            BlocListener<LoginCubit, BaseState>(
              listener: (_, state) {
                if (state is LoadedState) {
                  //  // TODO @nambuidanh
                  // goto main
                }
              },
              child: Container(),
            ),
            Container(
              child: CustomTextInput(
                validator: (String username) {
                  if (username.isEmpty) {
                    return "Hãy nhập tên đăng nhập!";
                  }
                  return "";
                },
                key: keyName,
                margin: EdgeInsets.symmetric(
                  horizontal: 10.sw,
                  vertical: 10.sw,
                ),
                enableBorder: true,
                obscureText: false,
                hideUnderline: true,
                hintText: "Nhập tên đăng nhập",
                title: "Tên đăng nhập",
              ),
            ),
            SizedBox(
              height: 10.sw,
            ),
            Container(
              child: CustomTextInput(
                validator: (String pass) {
                  if (pass.isEmpty) {
                    return "Hãy nhập pass!";
                  } else
                    return "";
                },
                key: keyPass,
                margin: EdgeInsets.symmetric(
                  horizontal: 10.sw,
                ),
                enableBorder: true,
                hideUnderline: true,
                prefixIcon: Padding(
                  padding: EdgeInsets.all(8.sw),
                  child: Icon(
                    Icons.lock,
                  ),
                ),
                isPasswordTF: true,
                suffixIcon: IconButton(
                  icon: Icon(Icons.visibility),
                  onPressed: () {},
                ),
                hintText: "Nhập mật khẩu",
                title: "Mật khẩu",
              ),
            ),
            SizedBox(
              height: 10.sw,
            ),
            BaseButton(
              onTap: () {
                String username = keyName.currentState?.value ?? "";
                String password = keyPass.currentState?.value ?? "";
                if (keyName.currentState!.isValid ||
                    keyPass.currentState!.isValid) {
                  BlocProvider.of<LoginCubit>(context)
                      .login(username, password);
                  Navigator.of(context).pushReplacementNamed(Routes.bottombar);
                  SharedPreferenceUtil.saveToken('token');
                }
              },
              margin: EdgeInsets.symmetric(horizontal: 30.sw, vertical: 10.sw),
              title: "Đăng nhập",
              width: 150.sw,
            ),
            Container(
              child: BaseButton(
                backgroundColor: Colors.transparent,
                title: "Quên mật khẩu?",
                textColor: AppColors.gray,
                onTap: (){
                  Navigator.of(context).pushNamed(Routes.forgotPassScreen);
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 50.sw, bottom: 20.sw),
              child: CustomTextLabel(
                "Chưa có tài khoản?",
                fontSize: 20,
                color: AppColors.gray,
              ),
            ),
            BaseButton(
              width: 150.sw,
              title: "Đăng ký",
              onTap: () {
                Navigator.pushNamed(context, Routes.registerScreen);
              },
            ),
          ],
        ),
      ),
    );
  }
}
