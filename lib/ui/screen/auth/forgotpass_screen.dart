import 'package:dashboard_app/res/resources.dart';
import 'package:dashboard_app/routes.dart';
import 'package:dashboard_app/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:scale_size/scale_size.dart';
class ForgotPassScreen extends StatefulWidget {
  const ForgotPassScreen({Key? key}) : super(key: key);
  @override
  State<ForgotPassScreen> createState() => _ForgotPassScreenState();
}

class _ForgotPassScreenState extends State<ForgotPassScreen> {
  GlobalKey<TextFieldState> oldPassKey = GlobalKey<TextFieldState>();
  GlobalKey<TextFieldState> newPassKey = GlobalKey<TextFieldState>();
  GlobalKey<TextFieldState> reNewPassKey = GlobalKey<TextFieldState>();
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      iconBackColor: AppColors.gray,
      title: "Đăng nhập",
      backgroundcolor: AppColors.white,
      body: Column(
        children: [
          SizedBox(height: 40.sw,),
          CustomTextInput(
            key: oldPassKey,
            validator: (String oldPass){
              if (oldPass.isEmpty)
                return "Hãy nhập mật khẩu cũ!";
              else
                return "";
            },
            enableBorder: true,
            hideUnderline: true,
            margin: EdgeInsets.symmetric(
              vertical: 10.sw,
              horizontal: 10.sw,
            ),
            title: "Mật khẩu cũ",
            isPasswordTF: true,
          ),
          SizedBox(height: 20.sw,),
          CustomTextInput(
              key: newPassKey,
              validator: (String newPass){
                if(newPass.isEmpty){
                  return "Hãy nhập mật khẩu mới!";
                }
                else
                  return "";
              },
              enableBorder: true,
              hideUnderline: true,
              margin: EdgeInsets.symmetric(
                vertical: 10.sw,
                horizontal: 10.sw,
              ),
              isPasswordTF: true,
              title: "Mật khẩu mới"
          ),
          SizedBox(height: 20.sw,),
          CustomTextInput(
              key: reNewPassKey,
              validator: (String reNewPass){
                if (reNewPass.isEmpty)
                  return "Hãy nhập lại mật khẩu mới!";
                else
                  return "";
              },
              enableBorder: true,
              hideUnderline: true,
              margin: EdgeInsets.symmetric(
                vertical: 10.sw,
                horizontal: 10.sw,
              ),
              isPasswordTF: true,
              title: "Nhập lại mật khẩu mới"
          ),
          SizedBox(height: 60.sw,),
          BaseButton(
            title: "Cập nhật",
            width: 200,
            onTap: (){
              // String oldPass = oldPassKey.currentState?.value ?? "";
              String newPass = newPassKey.currentState?.value ?? "";
              String reNewPass = reNewPassKey.currentState?.value ?? "";
              if(oldPassKey.currentState!.isValid || newPassKey.currentState!.isValid || reNewPassKey.currentState!.isValid){
                if (newPass != reNewPass){
                  CustomShowAlert(context, "Mật khẩu không trùng nhau!");
                }
                else{
                  Navigator.of(context).pushReplacementNamed(Routes.loginScreen);
                }
              }

            },
          ),
        ],
      ),
    );
  }
}

