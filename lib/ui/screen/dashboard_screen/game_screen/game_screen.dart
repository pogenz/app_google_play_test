// import 'package:dashboard_app/blocs/base_bloc/base_state.dart';
import 'package:dashboard_app/res/resources.dart';
import 'package:dashboard_app/blocs/cubit/cubit.dart';
// import 'package:dashboard_app/routes.dart';
import 'package:dashboard_app/ui/screen/screen.dart';
import 'package:dashboard_app/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scale_size/scale_size.dart';

import '../../../../blocs/base_bloc/base.dart';

class GameScreen extends StatelessWidget {
  const GameScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<GetAppCubit>(
      create: (_) => GetAppCubit(),
      child: GameBody(),
    );
  }
}

class GameBody extends StatefulWidget {
  @override
  _GameScreenState createState() => _GameScreenState();
}

class _GameScreenState extends State<GameBody> {
  List<Map> list = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<String> list1 = [
      AppImages.ASPHALT,
      AppImages.LQ,
      AppImages.GENSHIN,
      AppImages.PUBG,

    ];
    List<String> list2 = [
      AppImages.ASPHALTLOGO,
      AppImages.LQLOGO,
      AppImages.GENSHINLOGO,
      AppImages.PUBGLOGO
      ];
    return BaseScreen(
      customAppBar: CustomAppbar(),
      hiddenIconBack: true,
      backgroundcolor: AppColors.white,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            BlocListener<GetAppCubit, BaseState>(
                listener: (_,state){
                  if (state is LoadedState ){
                    // todo @Pogenz
                    //goto main
                  }
                },
                child: Container()
            ),
            TypeDeviceWidget(),
            Container(
              padding:
              EdgeInsets.only(left: 30.sw, top: 20.sw, bottom: 10.sw),
              width: double.infinity,
              decoration: BoxDecoration(color: AppColors.white),
              child: CustomTextLabel(
                "Trò chơi phổ biến",
                fontSize: 25,

              ),
            ),
            listgame(context, list1, list2),
            Container(
              padding: EdgeInsets.only(left: 30.sw, bottom: 10.sw),
              width: double.infinity,
              decoration: BoxDecoration(color: AppColors.white),
              child: CustomTextLabel(
                "Trò chơi hành động phiêu lưu",
                fontSize: 25
              )
            ),
            listgame(context, list1, list2),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              width: double.infinity,
              decoration: BoxDecoration(color: AppColors.white),
              child: CustomTextLabel(
                "Trò chơi có tính phí",
                fontSize: 25
              ),
            ),
            listgame(context, list1, list2),
          ],
        ),
      ),
    );
  }
}

Widget listgame(BuildContext context, List<String> list1, List<String> list2) {
  return Container(
    decoration: BoxDecoration(color: Colors.white),
    child:
    Padding(
      padding: EdgeInsets.only(
          left: 10.sw,
          top: 20.sw
      ),
      child: Container(
        height: 250.sw,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: list1.length,
            itemBuilder: (context, index) {
              return SizedBox(
                width: 250.sw,
                child: Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: AppColors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 1,
                              blurRadius: 3,
                              offset: Offset(
                                  0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        width: 250.sw,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(20),
                          child: Image.asset(
                            list1[index].toString(),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15.sw,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 1,
                                  blurRadius: 3,
                                  offset: Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ],
                            ),
                            width: 60.sw,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(20),
                              child: Image.asset(
                                list2[index].toString(),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 8,
                                horizontal: 8
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment
                                  .start,
                              mainAxisAlignment: MainAxisAlignment
                                  .start,
                              children: [
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                    vertical: 2.sw
                                  ),
                                  child: CustomTextLabel(
                                      "APP_Name",
                                      fontSize: 13,
                                  ),
                                ),
                                CustomTextLabel(
                                    "App_Style",
                                  fontSize: 13,
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 2.sw
                                  ),
                                  child: Row(
                                    children: [
                                      CustomTextLabel("4.0 ",fontSize: 13,),
                                      Icon(
                                        Icons.star,
                                        size: 14,
                                      )
                                    ],
                                  ),
                                )

                              ],
                            ),
                          ),


                        ],
                      ),

                    ],
                  ),
                ),
              );
            }),
      ),
    ),
  );
}
