import 'package:dashboard_app/blocs/base_bloc/base_state.dart';
import 'package:dashboard_app/res/resources.dart';
import 'package:dashboard_app/blocs/cubit/cubit.dart';
import 'package:dashboard_app/ui/screen/screen.dart';
import 'package:dashboard_app/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scale_size/scale_size.dart';
class AppScreen extends StatelessWidget {
  const AppScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<GetAppCubit>(
      create: (_) => GetAppCubit(),
      child: AppBody(),
    );
  }
}

class AppBody extends StatefulWidget {
  @override
  _AppScreenState createState() => _AppScreenState();
}

class _AppScreenState extends State<AppBody> {
  List<String> recommend = [
    AppImages.FACE,
    AppImages.YOUTUBE,
    AppImages.SPOTIFY,
    AppImages.CHROME
  ];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      customAppBar: CustomAppbar(),
      hiddenIconBack: true,
      backgroundcolor: AppColors.white,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: <Widget>[
            BlocListener<GetAppCubit, BaseState>(
                listener: (_,state){
                  if (state is LoadedState ){
                    // todo @Pogenz
                    //goto main
                  }
                },
                child: Container(),
            ),
            TypeDeviceWidget(),
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: 25.sw,
                  vertical: 20.sw
              ),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: AppColors.white
              ),
              child: CustomTextLabel(
                "Ứng dụng Xếp hạng hàng đầu",
                fontSize: 25,
                color: AppColors.black,
              ),
            ),
            listCommonApp(context),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 25.sw,
                vertical: 10.sw
              ),
              width: double.infinity,
              decoration: BoxDecoration(
                color: AppColors.white
              ),
              child: CustomTextLabel(
                "Đề xuất cho bạn",
                fontSize: 25,
                color: AppColors.black,
              ),
            ),
            listRecommendApp(context,recommend),
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: 25.sw
              ),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: AppColors.white
              ),
              child: CustomTextLabel(
                "Ứng dụng phổ biến",
                fontSize: 25,
                color: AppColors.black,
              ),
            ),
            listRecommendApp(context,recommend),
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: 25.sw
              ),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: AppColors.white
              ),
              child: CustomTextLabel(
                "Mạng xã hội",
                fontSize: 25,
                color: AppColors.black,
              ),
            ),
            listRecommendApp(context,recommend),
          ],
        ),
      ),
    );
  }
}
Widget listCommonApp(BuildContext context){
  List<String> list = [
    "Tiktok","Face","Mess"
  ];
  List<String> list1 = [
    AppImages.TIKTOK,
    AppImages.FACE,
    AppImages.MESS,
  ];
  return Container(
    padding: EdgeInsets.symmetric(
        horizontal: 25.sw
    ),
    decoration: BoxDecoration(
      color: AppColors.white,
    ),
    height: 350.sw,
    width: double.infinity,
    child: ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: 3,
        itemBuilder: (context, index){
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                iconImage(context, list1, index),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 12.sw
                      ),
                      child: CustomTextLabel(list[index].toString(),fontSize: 18),
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 12.sw),
                          child: Text('4',style: TextStyle(fontSize: 18),),
                        ),
                        Icon(Icons.star,size: 18,)
                      ],
                    )
                  ],
                ),

              ],
            ),
          );
        }),
  );
}
Widget listRecommendApp(BuildContext context, List<String> list){
  return Container(
    decoration: BoxDecoration(
      color: AppColors.white,
    ),
    child: Padding(
      padding: const EdgeInsets.all(20.0),
      child: SizedBox(
        height: 170.sw,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: list.length,
            itemBuilder: (context,index){
              return Container(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    iconImage(context, list, index),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 4.sw,
                          horizontal: 10.sw
                      ),
                      child: CustomTextLabel("App_Name"),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 10.sw
                      ),
                      child: Row(
                        children: [
                          CustomTextLabel("4 "),
                          Icon(Icons.star,size: 18,)
                        ],
                      ),
                    )
                  ],
                ),
              );
            }
        ),
      ),
    ),
  );
}
Widget iconImage (BuildContext context, List<String> list1,int index){
  return Container(
    padding: const EdgeInsets.all(4.0),
    decoration: BoxDecoration(
      color: AppColors.white,
      borderRadius: BorderRadius.circular(20),
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.3),
          spreadRadius: 1,
          blurRadius: 3,
          offset: Offset(0, 3), // changes position of shadow
        ),
      ],
    ),
    child: Container(
      width: 100,
      height: 100,
      padding: EdgeInsets.all(8),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(30),
        child: Image.asset(list1[index].toString(),fit: BoxFit.cover,),
      ),
    ),
  );
}