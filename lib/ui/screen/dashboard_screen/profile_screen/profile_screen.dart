// import 'package:dashboard_app/blocs/base_bloc/base_state.dart';
// import 'package:dashboard_app/blocs/cubit/updateuser_cubit.dart';
import 'package:dashboard_app/res/resources.dart';
import 'package:dashboard_app/routes.dart';
import 'package:dashboard_app/ui/widget/widget.dart';
import 'package:dashboard_app/utils/shared_preference.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scale_size/scale_size.dart';

// import '../../../../data/model/model.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      iconBackColor: AppColors.gray,
      backgroundcolor: AppColors.white,
      body: Center(
        child: Column(
          children: [
            // BlocListener<UpdateUserCubit,BaseState>(
            //   listener: (_, state) {
            //     if (state is LoadedState){
            //       //todo @Pogenz
            //       //goto main
            //     }
            //   },
            //   child: Container(),
            // ),
            CustomTextInput(
              align: TextAlign.center,
              maxLines: 1,
              width: 300.sw,
            ),
            SizedBox(
              height: 20.sw,
            ),
            CustomTextInput(
              align: TextAlign.center,
              maxLines: 1,
              width: 300.sw,
            ),
            SizedBox(
              height: 20.sw,
            ),
            CustomTextInput(
              align: TextAlign.center,
              maxLines: 1,
              width: 300.sw,
            ),
            SizedBox(
              height: 50.sw,
            ),
            BaseButton(
              width: 100.sw,
              title: "Sửa",
              onTap: (){
                // UserModel user = new UserModel();
                // BlocProvider.of<UpdateUserCubit>(context).update(user);
                Navigator.pushReplacementNamed(context, Routes.loginScreen);
                SharedPreferenceUtil.clearData();
              },
            ),
          ],
        ),
      ),
    );
  }
}
