import 'package:dashboard_app/ui/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:scale_size/scale_size.dart';

import '../../../res/resources.dart';

class TypeDeviceWidget extends StatefulWidget {
  const TypeDeviceWidget({Key? key}) : super(key: key);

  @override
  State<TypeDeviceWidget> createState() => _TypeDeviceWidgetState();
}

class _TypeDeviceWidgetState extends State<TypeDeviceWidget> {
  int? tappedIndex;
  List<Map> type = [
    {
      'name': 'Điện thoại',
      'icon': Icon(
        Icons.phone_android,
        color: AppColors.gray,
        size: 10,
      )
    },
    {
      'name': 'Máy tính bảng',
      'icon': Icon(Icons.tablet, color: AppColors.gray, size: 10)
    },
    {
      'name': 'Đồng hồ',
      'icon': Icon(Icons.watch, color: AppColors.gray, size: 10)
    },
    {'name': 'Ti vi', 'icon': Icon(Icons.tv, color: AppColors.gray, size: 10)},
    {
      'name': 'Xe ô tô',
      'icon': Icon(AppIcons.car, color: AppColors.gray, size: 10)
    },
  ];
  List<IconData> typeIcon = [
    Icons.phone_android,
    Icons.tablet,
    Icons.watch,
    Icons.tv,
    AppIcons.car
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tappedIndex = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.white,
      ),
      child: Padding(
        padding: EdgeInsets.only(left: 10.sw, top: 30.sw),
        child: SingleChildScrollView(
          child: Container(
            height: 45.sw,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: type.length,
                itemBuilder: (context, index) {
                  return SizedBox(
                    width: 170.sw,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Container(
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            alignment: Alignment.center,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(45.0),
                                side: BorderSide(
                                    width: 1.5, color: AppColors.gray1)),
                            primary: tappedIndex == index
                                ? AppColors.green
                                : AppColors.white,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                typeIcon[index],
                                color: AppColors.gray,
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 8.sw),
                                child: CustomTextLabel(type[index]['name'],
                                    fontSize: 14, color: AppColors.gray),
                              ),
                            ],
                          ),
                          onPressed: () {
                            setState(() {
                              tappedIndex = index;
                            });
                          },
                        ),
                      ),
                    ),
                  );
                }),
          ),
        ),
      ),
    );
  }
}
