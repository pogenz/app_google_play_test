// ignore_for_file: non_constant_identifier_names, prefer_const_constructors, avoid_unnecessary_containers, sized_box_for_whitespace

import 'dart:ui';

// import 'package:animations/animations.dart';
import 'package:dashboard_app/ui/screen/screen.dart';
import 'package:dashboard_app/utils/shared_preference.dart';
import 'package:flutter/material.dart';
// import 'package:dashboard_app/ui/screen/screen.dart';
import 'package:scale_size/scale_size.dart';

class BottomBar extends StatefulWidget {
  const BottomBar({Key? key}) : super(key: key);
  @override
  State<BottomBar> createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  int currentIndex = 0;
  int? tappedIndex;
  late var Screens;
  PageController controller = PageController(initialPage: 0);

  @override
  void initState(){
    // TODO: implement initState
    super.initState();
    Screens = [
      GameScreen(), AppScreen()];
    get();
    SharedPreferenceUtil.isKeepLogin();
    SharedPreferenceUtil.saveKeepLogin(true);
  }
  Future get() async {
    String token = await SharedPreferenceUtil.getToken();
    print(token);
  }

  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      extendBody: true,
      backgroundColor: Color.fromARGB(255, 6, 35, 59),
      body: Stack(
        children: <Widget>[
          PageView(
            onPageChanged: _onPageChanged,
            controller: controller,
            children: [
              GameScreen(), AppScreen()
            ],
          ),
        ],
      ),
      bottomNavigationBar: Container(
        child: Theme(
          data: Theme.of(context).copyWith(
            canvasColor:
            const Color.fromARGB(255, 243, 238, 238).withOpacity(0.8),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(
              vertical: 12.sw,
              horizontal: 25.sw
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(25),
              child: ClipRect(
                child: BackdropFilter(
                  filter: ImageFilter.blur(
                    sigmaX: 5,
                    sigmaY: 10,
                  ),
                  child: BottomNavigationBar(
                    type: BottomNavigationBarType.fixed,
                    selectedItemColor: const Color.fromARGB(255, 47, 100, 181),
                    selectedFontSize: 15,
                    unselectedFontSize: 15,
                    items: const <BottomNavigationBarItem>[
                      BottomNavigationBarItem(
                        icon: Icon(Icons.gamepad),
                        label: 'Trò chơi',
                      ),
                      BottomNavigationBarItem(
                        icon: Icon(Icons.widgets),
                        label: 'Ứng dụng'
                      ),
                      // BottomNavigationBarItem(
                      //     icon: Icon(Icons.person), label: 'Profile'),
                    ],
                    currentIndex: currentIndex,
                    onTap: _onPageChanged,

                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
  _onPageChanged(int index) {
    setState(() {
      controller.animateToPage(index,
          duration: const Duration(milliseconds: 200), curve: Curves.easeInOut);

      currentIndex = index;
    });
  }
}

