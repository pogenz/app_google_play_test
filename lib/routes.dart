import 'package:flutter/material.dart';
import 'package:dashboard_app/ui/screen/screen.dart';
import 'package:page_transition/page_transition.dart';


class Routes {
  Routes._();

  //screen name
  static const String splashScreen = "/splashScreen";
  static const String loginScreen = "/loginScreen";
  static const String mainScreen = "/mainScreen";
  static const String registerScreen = "/registerScreen";
  static const String dashboardScreen = "/dashBoardScreen";
  static const String bottombar = "/bottombar";
  static const String profile = "/profile";
  static const String gameScreen = "/gameScreen";
  static const String appScreen = "/appScreen";
  static const String profileScreen = "/profileScreen";
  static const String forgotPassScreen = "/forgotPassScreen";


  //init screen name
  static String initScreen() => splashScreen;

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case forgotPassScreen:
        return PageTransition(child: ForgotPassScreen(), type: PageTransitionType.rightToLeft);
      case profileScreen:
        return PageTransition(child: ProfileScreen(), type: PageTransitionType.rightToLeft);
      case gameScreen:
        return PageTransition(child: GameScreen(), type: PageTransitionType.rightToLeft);
      case profile:
        return PageTransition(child: ProfileScreen(), type: PageTransitionType.rightToLeft);
      case bottombar:
        return PageTransition(child: BottomBar(), type: PageTransitionType.rightToLeft);
      case appScreen:
        return PageTransition(child: AppScreen(), type: PageTransitionType.rightToLeft);
      case mainScreen:
        return PageTransition(child: MainScreen(), type: PageTransitionType.fade);
      case splashScreen:
        return PageTransition(child: SplashScreen(), type: PageTransitionType.fade);
      case loginScreen:
        return PageTransition(child: LoginScreen(), type: PageTransitionType.leftToRight);
      case registerScreen:
        return PageTransition(child: RegisterScreen(), type: PageTransitionType.rightToLeft);
      default:
        return MaterialPageRoute(builder: (context) => Container());
    }

  }
}
