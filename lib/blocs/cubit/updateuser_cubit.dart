import 'package:dashboard_app/data/network/network.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/model/model.dart';
import '../base_bloc/base.dart';
class UpdateUserCubit extends Cubit<BaseState>{
  UpdateUserCubit() : super(InitState());

  void update(UserModel userModel) async {
    try{
      emit(LoadingState());
      ApiResponse apiResponse = await Network().put(url: ApiConstant.update, body: userModel.toMap());
      if (apiResponse.isSuccess){
        emit(LoadedState(apiResponse.data));
      }
      else {
        emit(ErrorState(apiResponse.message));
      }
    }
    catch(e){
        emit(ErrorState(e.toString()));
    }
  }
}