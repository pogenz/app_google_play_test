import 'package:dashboard_app/blocs/base_bloc/base.dart';
import 'package:dashboard_app/data/model/user_model.dart';
import 'package:dashboard_app/data/network/network.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegisterCubit extends Cubit<BaseState> {
  RegisterCubit() : super(InitState());
  void register (UserModel userModel) async {
    emit(LoadingState());
    ApiResponse apiResponse = await Network().post(url: ApiConstant.register, body: userModel.toMap());
    try{
      if (apiResponse.isSuccess){
        emit(LoadedState(apiResponse.data));
      }
      else {
        emit(ErrorState(apiResponse.message));
      }
    }
    catch(e) {
      emit(ErrorState(e.toString()));
    }
  }
}
