import 'package:dashboard_app/blocs/base_bloc/base.dart';
import 'package:dashboard_app/data/network/network.dart';
import 'package:dashboard_app/utils/shared_preference.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginCubit extends Cubit<BaseState> {
  LoginCubit() : super(InitState());

  void login(String username, String pass) async {
   try{
     emit(LoadingState());
     ApiResponse apiResponse = await Network().post(url: ApiConstant.login, body: {"username": username, "pass": pass});
     if (apiResponse.isSuccess) {
       SharedPreferenceUtil.saveToken("token");
       emit(LoadedState(apiResponse.data));
     } else {
       emit(ErrorState(apiResponse.message));
     }
   }catch(e){
     emit(ErrorState(e.toString()));
   }
  }
}
