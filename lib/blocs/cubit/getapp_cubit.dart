import 'package:dashboard_app/blocs/base_bloc/base_state.dart';
import 'package:dashboard_app/data/model/model.dart';
import 'package:dashboard_app/data/network/network.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GetAppCubit extends Cubit<BaseState>{
  GetAppCubit() : super(InitState());
  void getapp(AppModel appModel) async {
    try{
      emit(LoadingState());
      ApiResponse apiResponse = await Network().get(url: ApiConstant.getapp);
      if (apiResponse.isSuccess){
        emit(LoadedState(apiResponse.data));
      }
      else{
        emit(ErrorState(apiResponse.message));
      }
    }
    catch(e){
      emit(ErrorState(e.toString()));
    }
  }
}